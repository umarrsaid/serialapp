import {StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  full: {
    flex: 1
  },
  body: {
    flex: 1
  },
  container: {
    flex: 1,
    marginTop: 20,
    marginLeft: 16,
    marginRight: 16
  },
  header: {
    display: "flex",
    justifyContent: "center"
    //alignItems: "center"
  },
  line: {
    display: "flex",
    flexDirection: "row"
  },
  line2: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  line3:{   
    padding: 1
  },
  title: {
    width: 100
  },
  value: {
    marginLeft: 20
  },
  valueparam:{
    fontSize:50,
    color:'white'
  },
  valuesat:{
    marginTop:40,
    fontSize:15,
    color:'white'
  },
  badge:{  
    justifyContent: "center",
    alignItems: 'center',
    marginTop:20,  
    marginLeft:30,
    height:80,
    width:200
  },
  badgesat:{  
    justifyContent: "center",
    alignItems: 'center',
    marginTop:20,  
    marginLeft:5,
    marginRight:30,
    height:80,
    width:70
  },
  output: {
    marginTop: 10,
    height: 300,
    padding: 10,
    backgroundColor: "#FFFFFF",
    borderWidth: 1
  },
  textInputStyle: { 
    marginBottom: 10,
    marginLeft: 5,
    marginRight: 25,
    width : 125,
    fontSize:20,
    height: 50,
    textAlign: "center",
    borderWidth:1,
    borderRadius:5,
  },
  picker: { 
    marginBottom: 10,
    marginLeft: 30,
    marginRight: 5,
    width : 175,
    fontSize:20,
    height: 50,
    textAlign: "center",
    borderWidth:1,
    borderRadius:5,
  },
  picker2: { 
    marginTop:1,
    fontSize:20,
    height: 50,
    textAlign: "center",
    borderWidth:1,
    borderRadius:5,
  },
  inputContainer: {
    marginTop: 10,
    borderBottomWidth: 2
  },
  textInput: {
    paddingLeft: 10,
    paddingRight: 10,
    height: 40
  },
  textChart: {
    marginTop: 7,
    marginLeft: 5,
    marginRight: 5,
    height: 40
  },
  buttonStart: {
    marginTop: 18,
    marginBottom: 16,
    marginLeft: 30,
    marginRight: 25,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#1ea873",
    borderRadius: 3
  },
  buttonStop: {
    marginTop: 18,
    marginBottom: 16,
    marginLeft: 30,
    marginRight: 25,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fb1414",
    borderRadius: 3
  },
  buttonDisable: {
    marginTop: 18,
    marginBottom: 16,
    marginLeft: 30,
    marginRight: 25,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#949494",
    borderRadius: 3
  },
  buttonclear: {
    marginTop: 5,
    marginBottom: 8,
    paddingLeft: 15,
    paddingRight: 15,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#147efb",
    borderRadius: 3
  },  
  buttonback: {
    marginTop: 5,
    marginBottom: 8,
    paddingLeft: 15,
    paddingRight: 15,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f29a3a",
    borderRadius: 3
  },  
  buttonclear2: {
    marginTop: 5,
    marginBottom: 8,
    paddingLeft: 15,
    paddingRight: 15,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#949494",
    borderRadius: 3
  },  
  button: {
    height: 40,
    justifyContent: "center",
    backgroundColor: "#147efb",
    borderRadius: 3
  },
  buttonText: {
    color: "#FFFFFF"
  },
  textArea:{
    height: 200,
    justifyContent: "flex-start" ,
    padding: 5
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});

export default styles;