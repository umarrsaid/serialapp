import { 
  StyleSheet,
  Dimensions
} from 'react-native';

let {
    width,
    height
} = Dimensions.get('window');

const styles = StyleSheet.create({
	form_container: {
		padding: 20
	},
	input: {
		height: 40,
		color: 'white',
		backgroundColor: 'rgba(0,0,0,0.2)',
		marginBottom: 10,
		paddingHorizontal: 10,
		marginHorizontal: 20,
		paddingLeft: 45,
		marginTop: 5,
	},
	form_title: {
		fontSize: 15,
	},
	buttonContainer: {
		backgroundColor: 'rgba(51,153,153,0.2)',
		paddingVertical: 15,
		marginHorizontal: 20,
		zIndex: 100,
	},
	buttonText: {
		textAlign: 'center',
		color: '#ffffff',
		fontWeight: '700'
	},
	signupTextCont: {
		flexGrow: 1,
		alignItems: 'flex-end',
		justifyContent: 'center',
		paddingVertical: 16,
		flexDirection: 'row'
	},
	tittle2: {
		justifyContent: 'center',
		flexDirection: 'row'
	},
	signupText: {
		color: 'gray',
		fontSize: 16,
		marginTop: 40,
		marginBottom: 10
	},
	signupButton: {
		color: 'rgba(168,0,0,100)',
		fontSize: 16,
		fontWeight: '500',
		marginTop: 40,
		marginBottom: 10
	},
	drawerImage: {
		height: 50,
		width: 50,
		alignItems: 'center',
		justifyContent: 'center',
	},
    picture: {
        flex: 1,
        width: width,
        height: height+30,
    },
    formContainer: {
        marginBottom: 30
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    title: {
        fontSize: 30,
        color: 'rgba(168,0,0,100)'
    },
    subtitle: {
        color: '#fff',
        marginTop: 10,
        width: 160,
        textAlign: 'center',
        opacity: 0.9
    },
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    animatedView: {
        width,
        backgroundColor: "#0a5386",
        elevation: 2,
        position: "absolute",
        bottom: 0,
        padding: 10,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
    },
    exitTitleText: {
        textAlign: "center",
        color: "#ffffff",
        marginRight: 10,
    },
    exitText: {
        color: "#e5933a",
        paddingHorizontal: 10,
        paddingVertical: 3
    }
});

export default styles;


