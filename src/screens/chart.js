import React, { Component } from "react";
import {LineChart} from "react-native-chart-kit";
import {
Text,
View, Dimensions, 
TouchableOpacity,
ScrollView,
Modal,
ActivityIndicator
} from "react-native";
import { Picker } from 'native-base';
const Item = Picker.Item;
import styles from '../css/styleLovibond';
import DatePicker from 'react-native-datepicker';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'rnSer.db' });
var arr=[];
var ars=[];
var chTime=['00:00','00:10','00:20','00:30','00:40','00:50','01:00'];
var chVle1 = [0,0,0,0,0,0,0]
var chVle2 = [0,0,0,0,0,0,0]
ars[1] = '°C'; ars[5]='pH'; ars[6]='%O2'; ars[7]='mg/L'; ars[13]='µs'; ars[14]='mS'; ars[18]='mV'; ars[19]='ppm';
var radio_props = [
    {label: 'pH', value: 5 },
    {label: '%O2', value: 6 },
    {label: 'mg/L', value: 7 },
    {label: 'µs', value: 13 },
    {label: 'mS', value: 14 },
    {label: 'mV', value: 18 },
    {label: 'ppm', value: 19 }
  ];
import dbTrx from '../others/dbTrx';
import moment from 'moment'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { setTimeout } from "core-js";

export default class Chart extends React.Component {
    kolam=[]
    constructor(props) {
        super(props);
        var d = new Date();        
        this.state = {
        date: new Date(),
        time1: moment(d).format('HH:mm'),
        time2: moment(d.setHours(d.getHours(),d.getMinutes()+60,0,0)).format('HH:mm'),
        wktu1: moment(d).format('HH:mm'),
        wktu2: moment(d.setHours(d.getHours(),d.getMinutes()+60,0,0)).format('HH:mm'),
        tm:'',
        refresh:'',
        satuan:5,
        kolam:'',
        jmlkolam:0,
        loading:false
      }
      
        this.loadChart = this.loadChart.bind(this);
    }
    componentDidMount() {    
        chVle1 = [0,0,0,0,0,0,0]
        chVle2 = [0,0,0,0,0,0,0]    
        const tgl = new Date(this.state.date).getFullYear()+'-'+(new Date(this.state.date).getMonth()+1)+'-'+new Date(this.state.date).getDate();
        const jam1 = this.state.time1+':00'
        const jam2 = this.state.time2+':00' 
        // this.input1bl()
        arr[1] = '°C'; arr[5]='pH'; arr[6]='%O2'; arr[7]='mg/L'; arr[13]='µs'; arr[14]='mS'; arr[18]='mV'; arr[19]='ppm';
                  
        // this.loadChart();
        this.setState({loading:true}) 
        chVle1 = [0,0,0,0,0,0,0]
        chVle2 = [0,0,0,0,0,0,0]
        setTimeout(() => {
            dbTrx.getTblItem('distinct(kolam)',0,null,' and sat="'+this.state.satuan+'" and tgl>="'+tgl+' '+(jam1.slice(0,2)+':00:00')+'" and tgl<="'+tgl+' '+(jam2.slice(0,2)+':00:00')+'"').then((result) => {
                if (result) {  
                    this.kolam=[]
                    this.kolam=result
                    this.setState({jmlkolam:result.length,loading:false})                  
                }
            })              
        }, 2000);
    }

    changeValue(item,value){
        //this.setState({loading:true})    
        var tx=''
        item=='time1'?tx='wktu1':tx='wktu2'

        this.setState({
            [tx]:this.state[item],
            tm:item.slice(-1)
        })

        this.setState({
            [item]:value
        })
       // console.log(this.state[item]+' '+this.state[tx]);
       this.setState({loading:true})
       chVle1 = [0,0,0,0,0,0,0]
       chVle2 = [0,0,0,0,0,0,0]
       setTimeout(() => {
        dbTrx.getTblItem('distinct(kolam)',0,null,' and sat="'+this.state.satuan+'" and tgl>="'+this.state.date+' '+(this.state.time1.slice(0,2)+':00:00')+'" and tgl<="'+this.state.date+' '+(this.state.time2.slice(0,2)+':00:00')+'"').then((result) => {
            if (result) {  
                this.kolam=[]
                this.kolam=result
                
                this.setState({jmlkolam:result.length,loading:false})   
                this.onValueChange('All')     
                }
            })  
        }, 2000);  
        
    }

    changeValue2(item,value){//kembali value awal
        var tx=''
        item=='time1'?tx='wktu1':tx='wktu2'

        this.setState({
            [tx]:this.state[item],
            tm:item.slice(-1)
        })

        this.setState({
            [item]:value
        })
       // console.log(this.state[item]+' '+this.state[tx]);
    }

    setRadio(value){
        //this.setState({loading:true}) 
        this.setState({satuan:value,loading:true})
        chVle1 = [0,0,0,0,0,0,0]
        chVle2 = [0,0,0,0,0,0,0]
        setTimeout(() => {
            dbTrx.getTblItem('distinct(kolam)',0,null,' and sat="'+this.state.satuan+'" and tgl>="'+this.state.date+' '+(this.state.time1.slice(0,2)+':00:00')+'" and tgl<="'+this.state.date+' '+(this.state.time2.slice(0,2)+':00:00')+'"').then((result) => {
                if (result) {  
                    this.kolam=[]
                    this.kolam=result
                    
                    this.setState({jmlkolam:result.length,loading:false})     
                }
            })            
        }, 2000);  
        //console.log(this.state.satuan);        
    }

    loadChart(){           
        // console.log('kolam');
        // console.log(this.state.kolam);  
        //console.log(this.state.satuan);  
        var kolam =''
        this.state.kolam !== '' ? (this.state.kolam !== 'All'? kolam=' and kolam="'+this.state.kolam+'" ' : kolam='') : kolam =''; // set kondisi kolam

        function makeTwoDigits (time) {
            const timeString = `${time}`;
            if (timeString.length === 2) return time
            return `0${time}`
        }

        chTime=[]              
        const tgl = new Date(this.state.date).getFullYear()+'-'+(new Date(this.state.date).getMonth()+1)+'-'+new Date(this.state.date).getDate();
        const jam1 = this.state.time1.slice(0,2) +':00:00'
        const jam2 = this.state.time2.slice(0,2) +':00:00' 

        //validasi selisih filter jam
        var ms = moment(tgl+' '+jam1,"YYYY/MM/DD HH:mm:ss").diff(moment(tgl+' '+jam2,"YYYY/MM/DD HH:mm:ss"));
        var d = moment.duration(ms);
        var s = (((d/1000)/60)/6)*(-1)

        // var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");
        // console.log('selisih '+jam1+' '+jam2+' '+s);        
        if((d/1000)>0) {
            this.changeValue2('time'+this.state.tm,this.state['wktu'+this.state.tm])
            alert('Jam ke-2 tidak boleh lebih kecil dari Jam ke-1')
            this.setState({loading:false})          
        } else {
            if((d/1000)<=(-3600)) { 
                var j = jam1.slice(0,2) 
                var k = jam2.slice(0,2)
                var l = k-j
                var h=0
                var z=0
                chTime.push(jam1.slice(0,2)+':00') //jampertama
                var txJam=moment(tgl+' '+jam1,"YYYY/MM/DD HH:mm:ss")  

                //membuat var sebelum dan sesudah
                var jmSblm = new Date(moment(tgl+' '+jam1,"YYYY/MM/DD HH:mm:ss"));
                var jmSsdh = new Date(moment(tgl+' '+jam2,"YYYY/MM/DD HH:mm:ss"));
                jmSblm.setHours(jmSblm.getHours(),jmSblm.getMinutes()-s,0,0)
                jmSsdh.setHours(jmSsdh.getHours(),jmSsdh.getMinutes()+s,0,0)
                jmSblm = makeTwoDigits(jmSblm.getHours())+':'+makeTwoDigits(jmSblm.getMinutes())
                jmSsdh = makeTwoDigits(jmSsdh.getHours())+':'+makeTwoDigits(jmSsdh.getMinutes())

                for (let i = 1; i < 7; i++) {                    
                    //i==6 ? chTime.push(jam2.slice(0,2)+':00') : ((10*i)==0?chTime.push(jam1.slice(0,2)+':00') : chTime.push(jam1.slice(0,2)+':'+(10*i)))                                                      
                    if(i==6) {
                        chTime.push(jam2.slice(0,2)+':00') 
                        h=h+1   
                        // this.setState({loading:false})  
                    } else {
                        var dt = new Date(txJam);
                        dt.setHours(dt.getHours(),dt.getMinutes()+s,0,0)
                        txJam=dt
                        //console.log(makeTwoDigits(dt.getHours())+':'+makeTwoDigits(dt.getMinutes()));                        
                        chTime.push(makeTwoDigits(dt.getHours())+':'+makeTwoDigits(dt.getMinutes())) 
                    }
                }
                chVle1=[0,0,0,0,0,0,0]                
                chVle2=[0,0,0,0,0,0,0]
                
                // dbTrx.getTblAvg(0,null,kolam+' and sat=1 and tgl>="'+tgl+' '+(jam1.slice(0,2)+':00:00')+'" and tgl<="'+tgl+' '+(jam2.slice(0,2)+':00:00')+'"','asc').then((result) => {
                //     if (result) {                         
                //         if (result.length>0) {
                //             chVle1=[]
                //             for (let i = 0; i < result.length; ++i) {
                //                 chVle1.push(result.item(i).value);
                //             } 
                //         } else {
                //             chVle1=[0,0,0,0,0,0,0] 
                //         }
                //     } 
                // }) 
                if(this.state.jmlkolam!=0){//data kosong
                    setTimeout(() => {
                        for (let x = 0; x < 7; x++) {                    
                            setTimeout(() => {                            
                                if (x==0) { //awal 
                                    // dbTrx.getTblAvg(1,null,kolam+' and sat=1 and tgl>="'+tgl+' '+(jam1.slice(0,2)+':00:00')+'" and tgl<="'+tgl+' '+(jam2.slice(0,2)+':00:00')+'"','asc').then((hasil) => {
                                    dbTrx.getTblAvg(1,null,kolam+' and sat=1 and tgl>="'+tgl+' '+(jmSblm+':00')+'" and tgl<="'+tgl+' '+(chTime[x]+':00')+'"','').then((hasil) => {
                                        chVle1=[]                               
                                        if (hasil.length>0) {  
                                            chVle1.push(hasil.item(0).value);
                                        } else {   
                                            chVle1.push(0);
                                        }
                                    })   
                                } else if (x==6) { //akhir
                                    dbTrx.getTblAvg(1,null,kolam+' and sat=1 and tgl>="'+tgl+' '+(chTime[x]+':00')+'" and tgl<="'+tgl+' '+(jmSsdh+':00')+'"','').then((hasil) => {
                                        if (hasil.length>0) {  
                                            chVle1.push(hasil.item(0).value); 
                                            //console.log(chVle1);     
                                        } else {  
                                            chVle1.push(0);
                                        }
                                    })        
                                } else {
                                    dbTrx.getTblAverage(0,null,kolam+' and sat=1 and tgl>="'+tgl+' '+(chTime[x-1]+':00')+'" and tgl<="'+tgl+' '+(chTime[x]+':00')+'"','').then((hasil) => {
                                        if (hasil!=0) {  
                                            chVle1.push(Number((hasil).toFixed(2)));
                                            // chVle2.push(x);
                                        } else {  
                                            chVle1.push(hasil);
                                        }
                                    })    
                                }                                                           
                            }, (x+1) * 1000);            
                        }                        
                    }, 2000);
                    
                    setTimeout(() => {
                        for (let y = 0; y < 7; y++) {                    
                            setTimeout(() => {
                                if (y==0) { //awal 
                                    dbTrx.getTblAvg(1,null,kolam+' and sat='+this.state.satuan+' and tgl>="'+tgl+' '+(jmSblm+':00')+'" and tgl<="'+tgl+' '+(chTime[y]+':00')+'"','asc').then((hasil) => {
                                        chVle2=[]                               
                                        if (hasil.length>0) {  
                                            chVle2.push(hasil.item(0).value);
                                        } else {   
                                            chVle2.push(0);
                                        }
                                        
                                    })   
                                } else if (y==6) { //akhir
                                    dbTrx.getTblAvg(1,null,kolam+' and sat='+this.state.satuan+' and tgl>="'+tgl+' '+(chTime[y]+':00')+'" and tgl<="'+tgl+' '+(jmSsdh+':00')+'"','desc').then((hasil) => {
                                        if (hasil.length>0) {  
                                            chVle2.push(hasil.item(0).value);
                                            this.setState({loading:false}) 
                                            //console.log(chVle2);     
                                        } else {  
                                            chVle2.push(0);
                                            this.setState({loading:false}) 
                                            //console.log(chVle2);     
                                        }
                                    })        
                                } else {
                                    dbTrx.getTblAverage(0,null,kolam+' and sat='+this.state.satuan+' and tgl>="'+tgl+' '+(chTime[y-1]+':00')+'" and tgl<="'+tgl+' '+(chTime[y]+':00')+'"','desc').then((hasil) => {
                                        if (hasil!=0) {  
                                            chVle2.push(Number((hasil).toFixed(2)));
                                            // chVle2.push(x);
                                        } else {  
                                            chVle2.push(hasil);
                                        }
                                    })    
                                }                                                           
                            }, (y+1) * 1000);            
                        }
                    }, 9000);    
                } else {
                    this.setState({loading:false})  
                }             

                // setTimeouut(() => {
                //     dbTrx.getTblAverage(0,null,kolam+' and sat='+this.state.satuan+' and tgl>="'+tgl+' '+(jam1.slice(0,2)+':00:00')+'" and tgl<="'+tgl+' '+(jam2.slice(0,2)+':00:00')+'"','asc').then((hasil) => {
                //         if (hasil) {                                              
                //             if (hasil.length>0) {              
                //                 // chVle2=[]                                
                //                 // for (let i = 0; i < hasil.length; ++i) {
                //                 //     chVle2.push(hasil.item(i).value);
                //                 // }  
                //                 // // console.log(chTime); 
                //                 // // console.log(chVle1);  
                //                 // // console.log('chVle2');  
                //                 // //console.log(chVle2); 
                //                 // // // dbTrx.getTbl('',0,null,'and tgl>="'+tgl+' '+(jam1.slice(0,2)+':00:00')+'" and tgl<="'+tgl+' '+(jam2.slice(0,2)+':00:00')+'"','asc').then((hasild) => {
                //                 // // //     if (hasild.length>0){ 
                //                 // // //         console.log('hasild')
                //                 // // //         console.log(hasild)
                //                 // // //     }
                //                 // // // })

                //                 // this.setState({loading:false}) 
                //             } else {
                //                 chVle2=[0,0,0,0,0,0,0]
                //                 //console.log(chVle2); 
                //                 this.setState({loading:false}) 
                //             } 
                //         } 
                //     })  
                // }, 500);    
            } else {
                this.changeValue2('time'+this.state.tm,this.state['wktu'+this.state.tm])
                alert('Periode jam yang dapat di pilih minimal 1 Jam')
                this.setState({loading:false}) 
            }
        } 
    }

    input1bl(){
        value = 
        "('2019-11-18 09:05:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 09:10:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 09:15:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 09:20:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 09:25:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 09:30:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 09:35:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 09:40:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 09:45:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 09:50:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 09:55:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 10:00:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 10:05:00',31.9,1,1,'R23','Lab'),"+
        "('2019-11-18 10:10:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 10:15:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 10:20:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 10:25:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 10:30:00',31.9,1,1,'R23','Lab'),"+
        "('2019-11-18 10:35:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 10:40:00',31.9,1,1,'R23','Lab'),"+
        "('2019-11-18 10:45:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 10:50:00',31.9,1,1,'R23','Lab'),"+
        "('2019-11-18 10:55:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 11:00:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 11:05:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 11:10:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 11:15:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 11:20:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 11:25:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 11:30:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 11:35:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 11:40:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 11:45:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 11:50:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 11:55:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 12:00:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 12:05:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 12:10:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 12:15:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 12:20:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 12:25:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 12:30:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 12:35:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 12:40:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 12:45:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 12:50:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 12:55:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 13:00:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 13:05:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 13:10:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 13:15:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 13:20:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 13:25:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 13:30:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 13:35:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 13:40:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 13:45:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 13:50:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 13:55:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 14:00:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 14:05:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 14:10:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 14:15:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 14:20:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 14:25:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 14:30:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 14:35:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 14:40:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 14:45:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 14:50:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 14:55:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 15:00:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 15:05:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 15:10:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 15:15:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 15:20:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 15:25:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 15:30:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 15:35:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 15:40:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 15:45:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 15:50:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 15:55:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 16:00:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 16:05:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 16:10:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 16:15:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 16:20:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 16:25:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 16:30:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 16:35:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 16:40:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 16:45:00',31.5,1,1,'R23','Lab'),"+
        "('2019-11-18 16:50:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 16:55:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 17:00:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 17:05:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 17:10:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 17:15:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 17:20:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 17:25:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 17:30:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 17:35:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 17:40:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 17:45:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 17:50:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 17:55:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 18:00:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 18:05:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 18:10:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 18:15:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 18:20:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 18:25:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 18:30:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 18:35:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 18:40:00',31.9,1,1,'R23','Lab'),"+
        "('2019-11-18 18:45:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 18:50:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 18:55:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 19:00:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 19:05:00',31.9,1,1,'R23','Lab'),"+
        "('2019-11-18 19:10:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 19:15:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 19:20:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 19:25:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 19:30:00',31.9,1,1,'R23','Lab'),"+
        "('2019-11-18 19:35:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 19:40:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 19:45:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 19:50:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 19:55:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 20:00:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 20:05:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 20:10:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 20:15:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 20:20:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 20:25:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 20:30:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 20:35:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 20:40:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 20:45:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 20:50:00',31.7,1,1,'R23','Lab'),"+
        "('2019-11-18 20:55:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 21:00:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 21:05:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 21:10:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 21:15:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 21:20:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 21:25:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 21:30:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 21:35:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 21:40:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 21:45:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 21:50:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 21:55:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 22:00:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 22:05:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 22:10:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 22:15:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 22:20:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 22:25:00',30.9,1,1,'R23','Lab'),"+
        "('2019-11-18 22:30:00',31.2,1,1,'R23','Lab'),"+
        "('2019-11-18 22:35:00',30.8,1,1,'R23','Lab'),"+
        "('2019-11-18 22:40:00',31.1,1,1,'R23','Lab'),"+
        "('2019-11-18 22:45:00',31.3,1,1,'R23','Lab')"
        dbTrx.insertTblVle(value).then((result) => {
            if (result) {
                    console.log(result);   
                                
            }            
        })            
    }

    loadData(){
       this.setState({loading:true})   
        setTimeout(() => {
            this.loadChart();
        }, 500);
    }

    onValueChange(value) {
        chVle1=[0,0,0,0,0,0,0]                
        chVle2=[0,0,0,0,0,0,0]
        this.setState({
          kolam : value
        });
     //this.setState({loading:true})          
    //   setTimeout(() => {
    //       this.loadChart();
    //   }, 500);
    }


    render() {          
        return (
            <ScrollView style={styles.body}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Modal
                            transparent={true}
                            animationType={'none'}
                            visible={this.state.loading}
                            onRequestClose={() => {console.log('close modal')}}>
                            <View style={styles.modalBackground}>
                                <View style={styles.activityIndicatorWrapper}>
                                <ActivityIndicator
                                    animating={this.state.loading} />
                                </View>
                            </View>
                        </Modal>
                        <View style={styles.line2}>
                            <DatePicker
                                style={{width: '35%'}}                                          
                                date={this.state.date}
                                mode="date"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateInput: {
                                    marginLeft: 36,
                                    width : 700,
                                    height : 40,
                                    alignItems: "center"                                      
                                    },
                                    dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                    }
                                }}
                                onDateChange={ (date) => this.changeValue('date',date) }
                                /> 
                                <Text style={styles.textChart}>From :</Text>
                                <DatePicker
                                style={{width: '20%'}}                                          
                                date={this.state.time1}
                                mode="time"
                                format="HH:mm"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateInput: {
                                    marginLeft: 5,
                                    width : 700,
                                    height : 40,
                                    alignItems: "center"                                      
                                    },
                                    dateIcon: {
                                    width:0,
                                    height:0,
                                    }
                                }}
                                onDateChange={ (date) => this.changeValue('time1',date) }
                                /> 
                                <Text  style={styles.textChart}>To :</Text>
                                <DatePicker
                                style={{width: '20%'}}                                          
                                date={this.state.time2}
                                mode="time"
                                format="HH:mm"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateInput: {
                                    marginLeft: 5,
                                    width : 700,
                                    height : 40,
                                    alignItems: "center"                                      
                                    },
                                    dateIcon: {
                                    width:0,
                                    height:0,
                                    }
                                }}
                                onDateChange={ (date) => this.changeValue('time2',date) }
                                /> 
                        </View>
                        <View style={styles.line3}>  
                            <View style={styles.picker2}>
                                <Picker
                                    mode='dropdown'
                                    enabled={this.timer ? false:true}
                                    selectedValue={this.state.kolam}
                                    onValueChange={this.onValueChange.bind(this)}>
                                    {   
                                        this.state.jmlkolam==0 ? <Item label={'Tidak ada data'} value={''} key={'1a'}/>:<Item label={'Semua Kolam'} value={'All'} key={'1b'}/>
                                    } 
                                    {
                                        this.kolam.map((item,index) => {                                            
                                            return <Item label={item.kolam} value={item.kolam} key={index}/>    
                                        })
                                        
                                    }
                                </Picker>
                            </View>
                        </View>  
                        <View style={styles.line3}>
                            <RadioForm
                                style={{marginTop: 10}}
                                formHorizontal={true}
                                animation={true}
                                >
                                {/* To create radio buttons, loop through your array of options */}
                                {
                                    radio_props.map((obj, i) => (
                                    <RadioButton labelHorizontal={true} key={i} >
                                        {/*  You can set RadioButtonLabel before RadioButtonInput */}
                                        <RadioButtonInput
                                        obj={obj}
                                        index={i}
                                        isSelected={this.state.satuan === obj.value}
                                        onPress={(value) => this.setRadio(value)}
                                        borderWidth={1}
                                        buttonInnerColor={'#e74c3c'}
                                        buttonOuterColor={this.state.satuan === obj.value ? '#2196f3' : '#000'}
                                        buttonSize={15}
                                        buttonOuterSize={15}
                                        buttonStyle={{}}
                                        buttonWrapStyle={{marginLeft: 5}}
                                        />
                                        <RadioButtonLabel
                                        obj={obj}
                                        index={i}
                                        labelHorizontal={true}
                                        onPress={(value) => this.setRadio(value)}
                                        labelStyle={{fontSize: 11, color: '#2d78e0'}}
                                        labelWrapStyle={{}}
                                        />
                                    </RadioButton>
                                    ))
                                }  
                            </RadioForm>
                        </View>
                        
                        <View style={styles.line3}>
                            <TouchableOpacity style={styles.buttonclear} onPress={() => this.loadData()}>
                            <Text style={styles.buttonText}>Load Data</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.line3}>
                            <Text style={styles.title}>Suhu:</Text>
                            <LineChart
                                data={{
                                labels: chTime,
                                datasets: [
                                    {
                                    data: chVle1
                                    }
                                ]
                                }}
                                width={Dimensions.get("window").width-50} // from react-native
                                height={220}
                                chartConfig={{
                                backgroundColor: "#00aae2",
                                backgroundGradientFrom: "#009ee2",
                                backgroundGradientTo: "#0f376e",
                                decimalPlaces: 2, // optional, defaults to 2dp
                                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                style: {
                                    borderRadius: 16
                                },
                                propsForDots: {
                                    r: "0",
                                    strokeWidth: "0",
                                    stroke: "#005ee2"
                                }
                                }}
                                bezier //type bezier chart
                                style={{
                                marginVertical: 4,
                                borderRadius: 8
                                }}
                            />
                        </View>
                        <View style={styles.line3}>
                            <Text style={styles.title}>{ars[this.state.satuan]}:</Text>
                            <LineChart
                                data={{
                                labels: chTime,
                                datasets: [
                                    {
                                    data: chVle2
                                    }
                                ]
                                }}
                                width={Dimensions.get("window").width-50} // from react-native
                                height={220}
                                chartConfig={{
                                backgroundColor: "#00aae2",
                                backgroundGradientFrom: "#009ee2",
                                backgroundGradientTo: "#0f376e",
                                decimalPlaces: 2, // optional, defaults to 2dp
                                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                style: {
                                    borderRadius: 16
                                },
                                propsForDots: {
                                    r: "0",
                                    strokeWidth: "0",
                                    stroke: "#005ee2"
                                }
                                }}
                                bezier //type bezier chart
                                style={{
                                marginVertical: 4,
                                borderRadius: 8
                                }}
                            />
                        </View>
                        <View style={styles.line3}>
                            <TouchableOpacity style={styles.buttonback} onPress={() => this.props.navigation.navigate('HomeStack')}>
                            <Text style={styles.buttonText}>Back To Home</Text>
                            </TouchableOpacity>
                        </View>
                
                    </View>
                </View>
            </ScrollView>
        )
    }
}