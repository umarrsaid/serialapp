import React, { Component } from "react";
import {
  Text,
  TextInput,
  View,
  TouchableOpacity,
  ScrollView,
  Alert,
  DeviceEventEmitter
} from "react-native";
import { Badge,Textarea, Picker } from 'native-base';
import NetInfo from "@react-native-community/netinfo";
import BackgroundTimer from 'react-native-background-timer';//'../others/timer';
import styles from '../css/styleLovibond';
import { RNSerialport, definitions, actions } from "react-native-serialport";
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'rnSer.db' });
const Item = Picker.Item;

import dbTrx from '../others/dbTrx';
import { setTimeout } from "core-js";

export default class Logger extends React.Component {
  timeGetValue
  timeSentAPI
  timer=false
  param1=''; param2=''
  paramSuhu=''; paramLain='';satSuhu=''; satLain='';satSuhu1=''; satLain1=''
  output=''
  logH=''
  btntext='Start'
  start = false
  
  constructor(props) {
    super(props);
    tmr=(1000*60)*5

    this.state = {
      timer:false,
      servisStarted: false,
      connected: false, //default false
      usbAttached: false,
      connection_Status: "Ofline",
      output:"",
      text:"",
      kolam:"",
      pressStatus:true,
      outputArray: [],
      baudRate: "9600",
      interface: "-1",
      sendText: "HELLO",
      returnedDataType: definitions.RETURNED_DATA_TYPES.HEXSTRING,
      selected1: 5,
    };    
    this.startUsbListener = this.startUsbListener.bind(this);
    this.stopUsbListener = this.stopUsbListener.bind(this);
  }
  
  componentDidMount() {
    // console.log('component did mount')
    this.startUsbListener()
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this._handleConnectivityChange.bind(this)
    );
 
    // Cek Koneksi
    NetInfo.isConnected.fetch().then(
      isConnected => {
        if (isConnected == true) {
          this.setState({ connection_Status: "Online" });
        } else {
          this.setState({ connection_Status: "Offline" });
        }
      }
    );
  }

  componentWillUnmount() {
    // console.log('component will unmount')
    this.stopUsbListener()
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this._handleConnectivityChange
    );
  }

  // Method cek koneksi
  _handleConnectivityChange = (isConnected) => {
    if (isConnected == true) {
      this.setState({ connection_Status: "Online" });
    } else {
      this.setState({ connection_Status: "Offline" });
    }
  };

  startUsbListener() {
    DeviceEventEmitter.addListener(
      actions.ON_SERVICE_STARTED,
      this.onServiceStarted,
      this
    );
    DeviceEventEmitter.addListener(
      actions.ON_SERVICE_STOPPED,
      this.onServiceStopped,
      this
    );
    DeviceEventEmitter.addListener(
      actions.ON_DEVICE_ATTACHED,
      this.onDeviceAttached,
      this
    );
    DeviceEventEmitter.addListener(
      actions.ON_DEVICE_DETACHED,
      this.onDeviceDetached,
      this
    );
    DeviceEventEmitter.addListener(actions.ON_ERROR, this.onError, this);
    DeviceEventEmitter.addListener(
      actions.ON_CONNECTED,
      this.onConnected,
      this
    );
    DeviceEventEmitter.addListener(
      actions.ON_DISCONNECTED,
      this.onDisconnected,
      this
    );
    DeviceEventEmitter.addListener(actions.ON_READ_DATA, this.onReadData, this);
    RNSerialport.setReturnedDataType(this.state.returnedDataType);
    RNSerialport.setAutoConnectBaudRate(parseInt(this.state.baudRate, 10));
    RNSerialport.setInterface(parseInt(this.state.interface, 10));
    RNSerialport.setAutoConnect(true);
    RNSerialport.startUsbService();
  };
  
  stopUsbListener = async () => {   
    DeviceEventEmitter.removeAllListeners();
    const isOpen = await RNSerialport.isOpen();
    if (isOpen) {
      Alert.alert("isOpen", isOpen);
      RNSerialport.disconnect();
    }
    RNSerialport.stopUsbService();
  };

  onServiceStarted(response) {
    //console.log('star')
    this.setState({ servisStarted: true });
    if (response.deviceAttached) {
      this.onDeviceAttached();
    }
  }
  onServiceStopped() {
    //console.log('stoped')
    this.setState({ servisStarted: false });
  }
  onDeviceAttached() {
    //console.log('attached')
    this.setState({ usbAttached: true });
  }
  onDeviceDetached() {
    //console.log('detached')
    this.setState({ usbAttached: false });    
  }
  onConnected() {
    //console.log('connect')
    this.setState({ connected: true });
    this.logH=this.logH+'Usb Terhubung'
  }
  onDisconnected() {
    //console.log('disconnect')
    this.setState({ connected: false });
    if (this.timer){ // jika usb terputus maka hapus timer
      BackgroundTimer.clearInterval(this.timeGetValue);
      BackgroundTimer.clearInterval(this.timeSentAPI);
      this.timer=false
      this.btntext='Start'
      this.paramSuhu = ""
      this.paramLain = ""
      this.satSuhu = ""
      this.satLain = ""
      this.satSuhu1 = ""
      this.satLain1 = ""
      this.logH=this.logH+'Usb tidak Terhubung'
      this.setState({pressStatus: true,output: "" })
    }
  }
  onReadData(data) {  
    if (
      this.state.returnedDataType === definitions.RETURNED_DATA_TYPES.INTARRAY
    ) {
      const payload = RNSerialport.intArrayToUtf16(data.payload);
      // this.setState({ output: this.state.output + payload });
      this.output = this.output + payload;
    } else if (
      this.state.returnedDataType === definitions.RETURNED_DATA_TYPES.HEXSTRING
    ) {
      const payload = RNSerialport.hexToUtf16(data.payload);
      // this.setState({ output: this.state.output + payload });
      this.output = this.output + payload;
    }   
    
    //console.log(this.output);
    if (this.output === "")  {
      this.start = false
      clearInterval(this.timeGetValue);
    }
  } 

  GetValue() {
    function shuffleArray(array) {
      let i = array.length - 1;
      for (; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
      }
      return array;
    }

    function addComma(index, string) {
      return string.substr(0, index) + '.' + string.substr(index,string.length);  
    }
     
    //4 2 01 0 1 00000264
    function convertValue(vle) {
  
      t1 = vle.substr(6,8)
      i = t1.length-parseInt(vle.substr(5,1))      
      t2 = addComma(i,t1);
  
      return t2
    };
                //BackgroundTimer.setInterval(() => {    BackgroundTimer.runBackgroundTimer(() => { 
    this.timeGetValue = BackgroundTimer.setInterval(() => { 
      //declare satuan     
      arr=[];
      arr[1] = '°C'; arr[5]='pH'; arr[6]='%O2'; arr[7]='mg/L'; arr[13]='µs'; arr[14]='mS'; arr[18]='mV'; arr[19]='ppm';

      // ambil 150 chr dari readData
      // asc=String.fromCharCode(13)+String.fromCharCode(10)
      // txt2 = shuffleArray(['42010100000276'+asc+'41060100000167'+asc+'42010100000275'+asc+'41060100000167'+asc+'42010100000275'+asc+'41060100000167'+asc+'42010100000275'+asc+'41060',
      //         '42010100000279'+asc+'41060100000164'+asc+'42010100000279'+asc+'41060100000164'+asc+'42010100000279'+asc+'41060100000164'+asc+'42010100000279'+asc+'41060',
      //         '42010100000274'+asc+'41060100000158'+asc+'42010100000274'+asc+'41060100000158'+asc+'42010100000274'+asc+'41060100000158'+asc+'42010100000274'+asc+'41060'
      //        ])[0]
      
      txt2 = this.output.substr(this.output.length-160,150)   //dari alat
      txt = txt2.split(String.fromCharCode(2)).join(';').split('\n').join('').split('\l').join('')
      length = txt.length
      last = txt.lastIndexOf(';')
      txt1 = txt.substr(last+1,length-last)
      // this.param1 = txt.substr(last-29,14)
      // this.param2 = txt.substr(last-14,14)

      this.param1 = txt.substr(last-31,14)
      this.param2 = txt.substr(last-15,14)
      a = this.param1
      b = this.param2
      //console.log(txt2);     
      try {
        sat1 = arr[parseInt(this.param1.substr(2,2))]
        sat2 = arr[parseInt(this.param2.substr(2,2))]
        satx = parseInt(this.param1.substr(2,2))
        saty = parseInt(this.param2.substr(2,2))

        if (typeof(sat1) !== 'undefined') { //deteksi value undefine
          if (typeof(sat2) !== 'undefined') {
            this.param1 = parseFloat(convertValue(this.param1))
            this.param2 = parseFloat(convertValue(this.param2))

            //penentuan param
            // console.log(a+' '+parseInt(a.substr(2,2)))  
            if (parseInt(a.substr(2,2))===1){
              this.paramSuhu = this.param1
              this.paramLain = this.param2
              this.satSuhu = sat1
              this.satLain = sat2
              this.satSuhu1 = satx
              this.satLain1 = saty
            } else if (parseInt(a.substr(2,2))===18){
              this.paramSuhu = ''
              this.paramLain = this.param1
              this.satSuhu = ''
              this.satLain = sat1
              this.satSuhu1 = ""
              this.satLain1 = satx
            } else if (parseInt(b.substr(2,2))===18){
              this.paramSuhu = ''
              this.paramLain = this.param2
              this.satSuhu = ''
              this.satLain = sat2
              this.satSuhu1 = ""
              this.satLain1 = saty
            } else {
              this.paramSuhu = this.param2
              this.paramLain = this.param1
              this.satSuhu = sat2
              this.satLain = sat1
              this.satSuhu1 = saty
              this.satLain1 = satx
            }
            //console.log('tes timer');
            //this.log = this.log+'Param lain '+this.paramLain+' Param Suhu '+this.paramSuhu+String.fromCharCode(13)+String.fromCharCode(10)
            //console.log('Param lain '+this.paramLain+' Param Suhu '+this.paramSuhu)
          }
        }
        this.setState({ output:""})
        this.output='' 
      }catch(err) {
        //
      } 
    }, 3000)
  }

  sentAPI(){ 
      //save ke database dan post API setiap 15 menit   
      // console.log(tmr);
      
      this.timeSentAPI = BackgroundTimer.setInterval(() => {
          if (!isNaN(this.paramLain)) {
            value = [dbTrx.getFullDate(),this.paramLain,this.satLain1,0,this.state.kolam,global.user]
            dbTrx.insertTbl(value).then((result) => {
              if (result) {
                this.logH = this.logH+value[0]+' Rekam Data Berhasil'+String.fromCharCode(13)+String.fromCharCode(10) 
              } else {
                this.logH = this.logH+value[0]+' Rekam Data Gagal'+String.fromCharCode(13)+String.fromCharCode(10)
              }
            })  
          }

          if (!isNaN(this.paramSuhu)) {
            setTimeout(() => { //atur selisih detik
              value = [dbTrx.getFullDate(),this.paramSuhu,this.satSuhu1,0,this.state.kolam,global.user]
              dbTrx.insertTbl(value).then((result) => {
                if (result) {
                  this.logH = this.logH+value[0]+' Rekam Data Berhasil'+String.fromCharCode(13)+String.fromCharCode(10) 
                } else {
                  this.logH = this.logH+value[0]+' Rekam Data Gagal'+String.fromCharCode(13)+String.fromCharCode(10)
                }
              })    
            }, 1000);
          } 
          
          // syncronisasi API
          dbTrx.getTbl('',50,0,'').then((result) => { // get column yang belum terupload
              if (result.length>0){ 
                for (let i = 0; i < result.length; ++i) {
                  setTimeout(() => { //di kasih jarak upload
                    NetInfo.isConnected.fetch().then((isConnected) => { //cek koneksi
                      if (isConnected) {
                          let data = {
                            "tgl_jam" : result[i]['tgl'],
                            "value":result[i]['value'],
                            "id_sat":result[i]['sat'],
                            "kolam":result[i]['kolam'],
                            "alat":result[i]['user']
                          }
                          dbTrx.sendApi(data).then((data) => {
                            if(data['Post']){
                              this.logH = this.logH+dbTrx.getFullDate()+' Sinkronisasi Berhasil'+String.fromCharCode(13)+String.fromCharCode(10) 
                              dbTrx.updateTbl(result[i]['id']) //update sync
                            }
                          }).catch((error)=>{
                              dbTrx.updateTbl(result[i]['id']) //update sync
                              // this.logH = this.logH+dbTrx.getFullDate()+' Sinkronisasi Gagal'+String.fromCharCode(13)+String.fromCharCode(10) 
                         });
                          
                      } else {
                        this.logH = this.logH+'Sinkronisasi Gagal, Tidak ada jaringan'+String.fromCharCode(13)+String.fromCharCode(10) 
                      }
                    });                     
                  }, 2000);
                } 
              }          
          })           
      }, tmr); //5mnt 300000 //15 mnt 900000 //(1000*60)*this.state.selected1)
  }

  onValueChange (value) {
      this.setState({
        selected1 : value
    });

    if (value==1) {
        tmr= 10000
    } else if (value==2){
        tmr= 15000
    } else {
        tmr= (1000*60)*value
    }
  }

  handleButton() {
    if (this.btntext=='Start') {     
      this.btntext='Stop'
      this.logH=this.logH+'Measuring is started'+String.fromCharCode(13)+String.fromCharCode(10)
      this.setState({pressStatus: false,output: "" })
      this.GetValue()  
      this.sentAPI()  
      this.timer=true
    } else {      
      // BackgroundTimer.clearInterval(this.timeGetValue);
      // BackgroundTimer.stopBackgroundTimer(this.timeGetValue);
      BackgroundTimer.clearInterval(this.timeGetValue);
      BackgroundTimer.clearInterval(this.timeSentAPI);
      this.timer=false
      this.btntext='Start'
      this.paramSuhu = ""
      this.paramLain = ""
      this.satSuhu = ""
      this.satLain = ""
      this.logH=this.logH+'Measuring is stoped'+String.fromCharCode(13)+String.fromCharCode(10)
      this.setState({pressStatus: true,output: "" })
    }
  }

  _onHideUnderlay() {
    this.setState({ pressStatus: false });
  }
  _onShowUnderlay() {
      this.setState({ pressStatus: true });
  }

  onError(error) {
    console.error(error);
  }

  handleConvertButton() {
    let data = "";
    if (
      this.state.returnedDataType === definitions.RETURNED_DATA_TYPES.HEXSTRING
    ) {
      data = RNSerialport.hexToUtf16(this.state.output);
    } else if (
      this.state.returnedDataType === definitions.RETURNED_DATA_TYPES.INTARRAY
    ) {
      data = RNSerialport.intArrayToUtf16(this.state.outputArray);
    } else {
      return;
    }
    this.setState({ output: data });
  }

  handleClearButton() {
    this.logH=''
    this.setState({ output: "" })
    this.setState({ outputArray: [] })
  }

  handleViewButton() {
    dbTrx.getTbl('',0,null,'').then((result) => {
      console.log(result)
      // console.log(result[1]['id'])
    })
  }

  handleDeleteButton() {
    dbTrx.deleteAllTbl().then((result) => {
     // console.log(result)
    })
  }

  buttonStyle = status => {
    return status
      ? styles.button
      : Object.assign({}, styles.button, { backgroundColor: "#C0C0C0" });
  };

  render() {  
    return (
      <ScrollView style={styles.body}>
        <View style={styles.container}>
           <View style={styles.header}>
              <View style={styles.line}>
                <Text style={styles.title}>Service:</Text>
                <Text style={styles.value}>
                    {this.state.servisStarted ? "Started" : "Not Started"}
                </Text>
              </View>
              <View style={styles.line}>
                <Text style={styles.title}>Usb:</Text>
                <Text style={styles.value}>
                    {this.state.usbAttached ? "Attached" : "Not Attached"}
                </Text>
              </View>
              <View style={styles.line}>
                <Text style={styles.title}>Connection:</Text>
                <Text style={styles.value}>
                    {this.state.connected ? "Connected" : "Not Connected"}
                </Text>
              </View>
              <View style={styles.line}>
                <Text style={styles.title}>Data:</Text>
                <Text style={styles.value}>
                    {this.state.connection_Status}
                </Text>
              </View>
              <View style={styles.line}>
                <Text style={styles.title}>Measuring:</Text>
              </View>
              <View style={styles.line2}>                 
                  <Badge info style={styles.badge}>        
                      <Text style={styles.valueparam}>
                          {this.paramLain != "" ? this.paramLain : ""}
                      </Text>    
                  </Badge>                 
                  <Badge info style={styles.badgesat}>      
                      <Text style={styles.valuesat}>
                          {this.satLain != "" ? this.satLain : ""}
                      </Text>
                  </Badge>
              </View>
              <View style={styles.line2}>       
                  <Badge info style={styles.badge}>  
                      <Text style={styles.valueparam}>
                          {this.paramSuhu != "" ? this.paramSuhu : ""}
                      </Text>
                  </Badge>
                  <Badge info style={styles.badgesat}>  
                      <Text style={styles.valuesat}>
                          {this.satSuhu != "" ? this.satSuhu : ""}
                      </Text> 
                  </Badge>    
              </View>
              <View style={styles.line3}>
                <TouchableOpacity disabled={this.state.connected ? (this.state.kolam !="" ? false : true) : true} style={this.state.connected ? (this.state.kolam !="" ? (this.state.pressStatus ? styles.buttonStart : styles.buttonStop) : styles.buttonDisable) : styles.buttonDisable} onPress={() => this.handleButton()}>
                  <Text style={styles.buttonText}>{this.btntext}</Text>
                </TouchableOpacity>
              </View> 
             <View style={styles.line2}>  
                <View style={styles.picker}>
                  <Picker
                      mode='dropdown'
                      enabled={this.timer ? false:true}
                      selectedValue={this.state.selected1}
                      onValueChange={this.onValueChange.bind(this)}>
                      <Item label='5 Second' value={1} />
                      <Item label='10 Second' value={2} />
                      <Item label='3 Minute' value={3} />
                      <Item label='5 Minute' value={5} />
                      <Item label='15 Minute' value={15} />
                      <Item label='30 Minute' value={30} />
                      <Item label='60 Minute' value={60} />
                  </Picker>
                </View>
                <TextInput
                    editable={this.timer ? false:true}
                    style={styles.textInputStyle}
                    placeholder="Kolam"
                    returnKeyLabel = {"next"}                    
                    onChangeText={(text) => this.setState({ kolam: text })}
                  />
             </View>             
              <View style={styles.line3}>
                  <Textarea bordered style={styles.textArea}>
                    {this.logH != "" ? this.logH : ""}
                  </Textarea>
              </View>
              <View style={styles.line3}>
                <TouchableOpacity style={styles.buttonclear} onPress={() => this.handleClearButton()}>
                  <Text style={styles.buttonText}>Clear Log</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.line3}>
                <TouchableOpacity disabled={this.state.pressStatus ? false : true} style={this.state.pressStatus ? styles.buttonclear : styles.buttonclear2} onPress={() => this.props.navigation.navigate('ChartStack')}>
                  <Text style={styles.buttonText}>View Data</Text>
                </TouchableOpacity>
                {/* <TouchableOpacity style={styles.buttonclear} onPress={() => this.handleDeleteButton()}>
                  <Text style={styles.buttonText}>Delete Data</Text>
                </TouchableOpacity> */}
              </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}