import React, { Component } from 'react';

const Config = {
    // base_url : 'http://192.168.1.34',
    base_url : 'https://logger.solusiteknis.com',
    statusBarColor: 'black',
    BgHeaderColor:'black',
    barStyle:'holo-content',
    iconColor:'white',
};


export default Config;