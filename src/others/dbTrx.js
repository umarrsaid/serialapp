import React, { Component } from "react";
import {AsyncStorage} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import NetInfo from "@react-native-community/netinfo";
import moment from 'moment';
var db = openDatabase({ name: 'rnSer.db' });
import config from './../others/config';

const dbTrx = {
    NetInfo:function(){
        NetInfo.isConnected.fetch().done(isConnected => {
            return isConnected         
        });
    },    

    getFullDate:function(){
        var that = this;
        // var date = new Date().getDate(); //Current Date
        // var month = new Date().getMonth() + 1; //Current Month
        // var year = new Date().getFullYear(); //Current Year
        // var hours = new Date().getHours(); //Current Hours
        // var min = new Date().getMinutes(); //Current Minutes
        // var sec = new Date().getSeconds(); //Current Seconds
        // Y/YYYY -> Year
        // M -> Month in single digit(Ex. 8)
        // MM -> Month in double-digit(Ex. 08)
        // MMM -> Month short name (Ex. Aug)
        // MMMM -> Month full name (Ex. August)
        // D -> Day in single digit(Ex. 5)
        // DD -> Day in single double-digit(Ex. 05)
        // HH -> Hours in 24 hr format
        // hh -> Hours in 12 hr format
        // mm -> Minutes
        // ss -> Seconds
        // a -> am/pm

        var date = moment()
        .utcOffset('+07:00')
        .format('YYYY-MM-DD HH:mm:ss');

        // return year + '-' + month + '-' + date + ' ' + hours + ':' + min + ':' + sec
        return date
    },

    getTbl:function(slc,lmt,sts,kond) { 
        if (lmt!==0) {limit='limit '+lmt} else {limit=''}        
        if (sts!==null) {sync='and sync='+sts} else {sync=''} 
        if (kond!=='') {knd=kond} else {knd=''}
        if (slc!=='') {select=slc} else {select='*'}
        return new Promise(function(resolve,reject) {
            db.transaction(function(tx){ 
                tx.executeSql(
                    'SELECT '+select+' FROM data_log where sat<>"" and value<>"" '+knd+' '+sync+' order by id asc '+limit,
                    [],
                    (tx, results) => {  
                        var temp = [];
                        for (let i = 0; i < results.rows.length; ++i) {
                        temp.push(results.rows.item(i));
                        }         
                        resolve(temp)
                    }
                );
            });
        });
    },

    getTblItem:function(slc,lmt,sts,kond) {         
        if (lmt!==0) {limit='limit '+lmt} else {limit=''}        
        if (sts!==null) {sync='and sync='+sts} else {sync=''} 
        if (kond!=='') {knd=kond} else {knd=''}
        if (slc!=='') {select=slc} else {select='*'}
        return new Promise(function(resolve,reject) {
            db.transaction(function(tx){ 
                tx.executeSql(
                    'SELECT '+select+' FROM data_log where sat<>"" and value<>"" '+knd+' '+sync+' order by id asc '+limit,
                    [],
                    (tx, results) => {  
                        var temp = [];
                        for (let i = 0; i < results.rows.length; ++i) {              
                            temp.push(results.rows.item(i));
                        }         
                        resolve(temp)
                    }
                );
            });
        });
    },

    getTblAvg:function(lmt,sts,kond,order) { 
        if (lmt!==0) {limit='limit '+lmt} else {limit=''}        
        if (sts!==null) {sync='and sync='+sts} else {sync=''} 
        if (kond!=='') {knd=kond} else {knd=''}
        //console.log(kond)
        return new Promise(function(resolve,reject) {
            db.transaction(function(tx){ 
                tx.executeSql(
                    'SELECT value FROM data_log where sat<>"" and value<>"" '+knd+' '+sync+' order by id '+order+' '+limit,
                    [],
                    (tx, results) => {                                 
                        resolve(results.rows)
                    }
                );
            });
        });
    },

    getTblAverage:function(lmt,sts,kond,order) { 
        if (lmt!==0) {limit='limit '+lmt} else {limit=''}        
        if (sts!==null) {sync='and sync='+sts} else {sync=''} 
        if (kond!=='') {knd=kond} else {knd=''}
        //console.log(kond)
        return new Promise(function(resolve,reject) {
            db.transaction(function(tx){ 
                tx.executeSql(
                    'SELECT AVG(value) as value FROM data_log where sat<>"" and value<>"" '+knd+' '+sync+' order by id '+order+' '+limit,
                    [],
                    (tx, results) => {    
                        //console.log(results.rows.item(0)['value'])     
                        results.rows.item(0)['value'] !== null ? resolve(results.rows.item(0)['value']) : resolve(0)
                        
                    }
                );
            });
        });
    },

    insertTbl:function(values) { 
        return new Promise(function(resolve,reject) {
            db.transaction(function(tx){ 
                tx.executeSql(
                    'INSERT INTO data_log (tgl, value, sat, sync, kolam, user) VALUES (?,?,?,?,?,?)',
                    [values[0], values[1], values[2],values[3],values[4],values[5]],
                    (tx, results) => {                         
                    if (results.rowsAffected > 0) {   
                        //console.log(values[0]+' '+values[1]+' '+ values[2]+' '+values[3]+' '+values[4]+' berhasil')
                        resolve(true)
                    } else {
                        resolve(false)
                        //console.log('gagal');
                    }
                    }
                );
            });
        });
    },

    insertTblVle:function(values) { 
        console.log('tes');
        
        return new Promise(function(resolve,reject) {
            db.transaction(function(tx){ 
                tx.executeSql(
                    'INSERT INTO data_log (tgl, value, sat, sync, kolam, user) VALUES '+values,
                    [],
                    (tx, results) => {                         
                    if (results.rowsAffected > 0) {   
                        //console.log(values[0]+' '+values[1]+' '+ values[2]+' '+values[3]+' '+values[4]+' berhasil')
                        resolve(true)
                    } else {
                        resolve(false)
                        //console.log('gagal');
                    }
                    }
                );
            });
        });
    },

    updateTbl:function(id) { 
        return new Promise(function(resolve,reject) {
            db.transaction(function(tx){ 
                tx.executeSql(
                    'UPDATE data_log set sync=? where id=?',
                    [1, id],
                    (tx, results) => {                        
                    if (results.rowsAffected > 0) {   
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                    }
                );
            });
        });
    },

    deleteAllTbl:function() { 
        return new Promise(function(resolve,reject) {
            db.transaction(function(tx){ 
                tx.executeSql('DROP TABLE IF EXISTS data_log', [],
                    (tx, results) => {
                        if (results.rowsAffected = 0) {   
                            resolve(false)
                        } else {
                            resolve(true)
                        }
                    }
                );
                tx.executeSql(
                'CREATE TABLE IF NOT EXISTS data_log(id INTEGER PRIMARY KEY AUTOINCREMENT, tgl TIMESTAMP, value FLOAT, sat INT, sync TINYINT(4), kolam VARCHAR(40), user VARCHAR(40))',
                []
                );
            });
        });
    },

    sendApi:function(value) {
        const token = global.token
        // console.log(token)
        // console.log(value)
        let data = {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(value),
            headers: {
            'Accept':       'application/json',
            'Content-Type': 'application/json',              
            // 'Authorization': 'Bearer ' + token
            'X-CSRFToken':  token
            }
        }
    
        return fetch(config.base_url+'/api/post-alat', data)
        .then(function(response){
            return response.json();
        })
        .then(function(json){
            return {
            Post: true,
            }
        })
        .catch(function(error) {
        console.log('There has been a problem with your fetch operation: ' + error.message);
        // ADD THIS THROW error
            throw error;
        })
    }
}

export default dbTrx;