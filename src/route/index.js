import {createAppContainer,createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack'
import Login from './login';
import Loading from './loading';
import Main from './../screens/logger';
import Chart from './../screens/chart';


const AuthRoute = createStackNavigator({
    Login:{screen:Login,
        headerMode: 'none',
        navigationOptions: {
            header: null,
        }},
});

const MainRoute = createStackNavigator({
    Home:{screen:Main,
        headerMode: 'none',
        navigationOptions: {
            header: null,
        }},
});

const ChartRoute = createStackNavigator({
    Chart:{screen:Chart,
        headerMode: 'none',
        navigationOptions: {
            header: null,
        }},
});

const StackNavigator = createSwitchNavigator(
    {
        AuthStack:{screen:AuthRoute},
        HomeStack:{screen:MainRoute},
        ChartStack:{screen:ChartRoute},
        Loading:{screen:Loading}
    },
    {
        initialRouteName:'Loading'
    }
);

const AppContainer = createAppContainer(StackNavigator);

export default AppContainer;