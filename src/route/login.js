import React, {Component} from 'react';
import {View,
    ImageBackground,
    Image,
    StatusBar, TextInput, Text,ActivityIndicator,
    TouchableOpacity,ToastAndroid,AsyncStorage} from 'react-native';
// import AsyncStorage from '@react-native-community/async-storage';
import {encrypt} from 'react-native-simple-encryption';
import { getBrand, getCarrier, getIpAddress } from 'react-native-device-info';

import bgSrc from './../css/img/bg-login.jpg';
import logo from './../css/img/labicon.png';

import styles from './../css/loginCss';
import config from './../others/config';



export default class Login extends Component{

    static navigationOptions = {
        title:'Login'
    }

    constructor(props){
        super(props);
        this.state = {username:'',password:'',imei:'',token:'','isLoading':false}
    }
    
    checkLogin = async () => {
        this.setState({
            imei: global.imei,
            'isLoading':true
        })        
        const {username,password} = this.state;
        const addr = getIpAddress()
        const brand = getBrand()
        const carrier = getCarrier()

        //console.log(addr+' '+brand+' '+global.imei);
        if (username=="") {
			ToastAndroid.show('Silakan isi nama perangkat!', ToastAndroid.LONG);
		}
		else{
            try {   //?username='+username+'&password='+password+'&imei='+imei+'
            // console.log(global.imei)
                let response = await fetch(config.base_url+'/api/login?username='+username+'&password='+password+'&imei='
                    +global.imei+'&ip_address='+addr+'&brand='+brand+'&carrier='+carrier,{
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                    // ,
                    // body: JSON.stringify({
                    // 	username: username,
                    // 	password: password,
                    //     imei: global.imei,
                    //     ip_address: addr,
                    //     brand: brand,
                    //     carrier: carrier
                    // })
                });
                 console.log(response);
                
                let res = await response.json();
                if (response.status == 200) {	
                    // AsyncStorage.setItem('token',encrypt('key123',imei));
                    this.setState({
                        'isLoading':false
                    }) 
                    AsyncStorage.multiSet([
                        ['username',username],
                        ['auth',encrypt('key123',global.imei)],
                        ['token',res.token]
                    ])
                    this.props.navigation.navigate('Loading');
                    console.log('success');
                } else {
                    AsyncStorage.clear();
                    console.log('====================================');
                    console.log(res);
                    console.log('====================================');
                    this.setState({
                        'isLoading':false
                    }) 
                    ToastAndroid.show('Nama perangkat yang Anda masukkan salah atau Imei tidak terdaftar !', ToastAndroid.LONG);
                }
			} catch(errors) {
                console.log(errors);
                this.setState({
                    'isLoading':false
                }) 
					ToastAndroid.show('Jaringan bermasalah , silahkan coba lagi', ToastAndroid.LONG);
			}
		}
    }

    // render(){
    //     return<View style={style.fullScreenView}>
    //         <TextInput placeholder="Enter Username"  style={style.textInput} 
    //         value={this.state.username} keyboardType='default' 
    //         onChangeText={(text) => this.setState({username:text})}
    //         />
    //         <TextInput placeholder="Enter Password"  style={style.textInput} 
    //         value={this.state.password} keyboardType='default' secureTextEntry={true}
    //         onChangeText={(text) => this.setState({password:text})}
    //         />
    //         <Button title="Login" onPress={this.checkLogin} />
    //     </View>
    // }

    render() {
        let button = <TouchableOpacity style={styles.buttonContainer} onPress={this.checkLogin.bind(this)}  >
                {this.state.isLoading ? <ActivityIndicator color="white" /> : <Text style={styles.buttonText}>Masuk</Text>} 
            </TouchableOpacity>;
            // if(this.state.isLoading){
            // button = <TouchableOpacity disabled={true} style={styles.buttonContainer} onPress={this.checkLogin.bind(this)}  >
            //     <View style={{alignItems: 'center'}}>
            //         <ActivityIndicator color="white" />
            //     </View>
            // </TouchableOpacity>;
            // }
        return (		
            <ImageBackground source={bgSrc} style={styles.picture} >
                <View style={styles.logoContainer}>
                    <StatusBar
                        backgroundColor='transparent'
                        barStyle="dark-content"
                    />
                    {/* {this.state.isKeyboard ? null : <Image style={{  width: 90 , height: 140, marginBottom: 15}} source={logo} /> } */}
                    <Image style={{  width: 250 , height: 252,}} source={logo} />
                </View>
                <View style={styles.formContainer}>
                    <View style={styles.form_container}>
                        <StatusBar
                            backgroundColor='white'
                            barStyle="dark-content"
                        />
                        <TextInput style={styles.input} 
                            onChangeText={(val) => this.setState({username: val})}
                            placeholder="Username"
                            placeholderTextColor='gray'
                            returnKeyType='next'
                            autoCorrect={false}
                            underlineColorAndroid="transparent"
                            onSubmitEditing={()=> this.refs.txtPassword.focus()}
                        >
                        </TextInput>
                        <TextInput style={styles.input}
                            onChangeText={(val) => this.setState({password: val})}
                            placeholder="Password"
                            placeholderTextColor='gray'
                            returnKeyType='go'
                            secureTextEntry
                            autoCorrect={false}
                            underlineColorAndroid="transparent"
                            ref={"txtPassword"}
                            >
                        </TextInput>
                        {button}
                    </View>
                </View>
            </ImageBackground>
        );
      }
}

// const style = StyleSheet.create({
//     textInput:{
//         width:200,
//         height:40,
//         borderWidth:1,
//         borderColor:'green',
//         borderRadius:4,
//         padding:5,
//         margin:10
//     },
//     fullScreenView:{
//         flex:1,
//         justifyContent:'center',
//         alignItems:'center'
//     }
// });
