import React, {Component} from 'react';
import {View,PermissionsAndroid,AsyncStorage} from 'react-native';
// import AsyncStorage from '@react-native-community/async-storage';
import {decrypt} from 'react-native-simple-encryption';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'rnSer.db' });

export async function request_READ_PRIVILEGED_PHONE_STATE() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_PRIVILEGED_PHONE_STATE ,
        {
          'title': 'ReactNativeCode wants to READ_PRIVILEGED_PHONE_STATE',
          'message': 'ReactNativeCode App needs access to your personal data. '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
   
      }
      else {
   
        Alert.alert("Permission Not Granted");
   
      }
    } catch (err) {
      console.warn(err)
    }
}

export default class Loading extends Component{

    constructor(props){     
        super(props);   
        global.imei=''
        this.checkIfLoggedInOrNot();

        db.transaction(function(txn) {
            txn.executeSql(
              "SELECT name FROM sqlite_master WHERE type='table' AND name='data_log'",
              [],
              function(tx, res) {
                if (res.rows.length == 0) {
                  txn.executeSql('DROP TABLE IF EXISTS data_log', []);
                  txn.executeSql(
                    'CREATE TABLE IF NOT EXISTS data_log(id INTEGER PRIMARY KEY AUTOINCREMENT, tgl TIMESTAMP, value FLOAT, sat INT, sync TINYINT(4), kolam VARCHAR(40), user VARCHAR(40))',
                    []
                  );
                }
              }
            );
          });
    }

    async componentDidMount() {
		await request_READ_PRIVILEGED_PHONE_STATE()
		const IMEI = require('react-native-imei');
	 
	    var IMEI_2 = IMEI.getImei();
	    IMEI.getImei().then(imeiList => {
            global.imei=imeiList[0]
        });	
    }

    checkIfLoggedInOrNot = async () => {
        const granted = await PermissionsAndroid.request(
			PermissionsAndroid.PERMISSIONS.READ_PRIVILEGED_PHONE_STATE ,
			{
				'title': 'Aplikasi ini membutuhkan izin READ_PRIVILEGED_PHONE_STATE',
				'message': 'Untuk mendapatkan imei '
			}
		)
		if (granted === PermissionsAndroid.RESULTS.GRANTED) {
		// Alert.alert("Permission Granted.");
		}
		else {
			Alert.alert("Izin Tidak Diberikan");
        }
         
        //await AsyncStorage.setItem('auth',''); //reset token
        const auth = await AsyncStorage.getItem('auth');
        global.user = await AsyncStorage.getItem('username');
        global.token = await AsyncStorage.getItem('token');
        setTimeout(() => {
            if (auth!=null){
                if (decrypt('key123',auth)==global.imei){
                //logged in
                 this.props.navigation.navigate('HomeStack');
                }
            }else{
                // yet to login
                 this.props.navigation.navigate('AuthStack');
            } 
        }, 100);        
    }


    render(){
        return<View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        </View>
    }
}
